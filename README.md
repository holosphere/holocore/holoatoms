# holoAtoms


framaPad : https://annuel2.framapad.org/p/holoAtoms

(from WP)

holoAtoms is the projected memory of consciousness unit (all information / thoughts recorded on holoRings are stored as single files : holoAtom) 

On IPFS holoAtoms are snapshots of the mutables data on GIT repository there they are always derived from the Gitsource.

Each holoAtom have similar structure i.e. it has some necessary holoAssets such as: 
- Metadata (keywords,tags, labels …)
- Multimedia (images, video, …)
- slides, abstract, webpage, presentation, PDF,
- Research papers, articles, whitepapers, documents, 
- References

The holoAtoms is present at 3 locations :  

* IPFS (immutable) particum
* GIT repository (centralized) partika
* local working version (parikis) ( to be validated see question line38 ?)


### holoAtom media assets :
    
    * icons
    * audio (audible version of the README (via "text to speech" )
    * with associated cymatic images
    * video tutorial, howto ...

(to keep a link to the source : sign by creating ring on the KIN -> people == life-source )       

